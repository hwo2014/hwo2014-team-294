#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>
struct turn {
	turn();
	double distance; ///distance to start of vip curve
	double angle;  ///vip curve angle
	double radius;
	int pieceid_start;
	int pieceid_end;
//		piece::piece_type current_type;

	int id() const { return pieceid_start; }

	double get_length(int lane) const;

	double length_percent(int piece_index, double piece_distance , int lane_index) const;

	void dump(std::string prefix) const;
	bool is_left() const;
	bool is_right() const;
	double get_speed_limit(int lane, double Fslip) const;
};

struct turns: public std::vector<turn> {
	void dump() const;
	const_iterator find(int piece) const;
};


struct lane {
	lane();

	double distance_from_center;
};

struct lanes: public std::map<int,lane> {
	int max_pos() const;
	void dump() const;
};


struct piece {
	piece();
	virtual ~piece();

	enum lane_pos {
		lane_pos_central,
		lane_pos_interior,
		lane_pos_exterior,
	};

	static std::string lane_pos_str(const lane_pos& lp);

	enum piece_type {
		piece_type_straight,
		piece_type_curve,
	};
	
	static std::string piece_type_str(piece_type pt);
	virtual piece_type type() const=0;
	virtual lane_pos get_lane_pos(double lateral_pos) const=0;
	virtual void dump(const lanes& lanes_) const=0;
	virtual double length(double lateral_pos, bool consideer_switch) const=0;  ///position from the center of the piece axis

	bool _switch;
};

struct straight:public piece {
	straight(double length);
	virtual double length(double lateral_pos, bool consideer_switch) const;

	double _length;
	double _length_switch;

	virtual piece_type type() const;

	virtual void dump(const lanes& lanes_) const;

	lane_pos get_lane_pos(double lateral_pos) const;
};

struct curve:public piece {
	curve(double angle, double radius);
	virtual piece_type type() const;

	virtual double length(double lateral_pos, bool consideer_switch) const;
	virtual void dump(const lanes& lanes_) const;

	lane_pos get_lane_pos(double lateral_pos) const;
	double _angle; ///deg
	double _radius;


	double _length_switch;
};

struct track: public std::map<int,piece*> {
	track();
	~track();

	void dump() const;
	const_iterator find_switch(int startpiece, int endpiece) const;
	double get_length(int piece, int lanendx, bool using_switch);
	turns get_turns() const;
	turn get_next_turn(int ln, int piece_index, double piece_distance) const;

	int get_index(const_iterator i) const {
		return std::distance(begin(), i);
	}

	const_iterator next(const_iterator) const;
	int next(int) const;
	int shortest_lane(const_iterator, const_iterator) const;

	const_iterator find_switch(int piecedesde) const;
	const_iterator find_switch(const_iterator piecedesde) const;

	struct lanesdist: public std::map<int,double> {
		void dump() const;
	};

	lanesdist get_lanesdist(const_iterator from, const_iterator to) const;

	struct switches: public std::vector<track::const_iterator> {
		void dump() const;
	};

	switches get_switches() const;


	std::string name;
	lanes _lanes;
};


struct data {
	data() : angle(0), piece_distance(0), start_lane_index(0), end_lane_index(0), lap(0), piece_index(0) {
	}

	double angle;
	double piece_distance;
	int piece_index;
	int start_lane_index;
	int end_lane_index;
	int lap;
	bool using_switch() const { return start_lane_index!=end_lane_index; }
};

class game_logic {
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_join_race(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_spawn(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
	msg_vector your_car(const jsoncons::json& data);
	msg_vector game_init(const jsoncons::json& data);
msg_vector finish(const jsoncons::json& data);
msg_vector on_turbo_available(const jsoncons::json& data);
msg_vector on_turbo_start(const jsoncons::json& data);
msg_vector on_turbo_end(const jsoncons::json& data);

msg_vector lapFinished(const jsoncons::json& data);
msg_vector tournamentEnd(const jsoncons::json& data);
msg_vector disqualified(const jsoncons::json& data);

bool turbonow;

public:

	double speed(const data& data_);
	double throttle(const data& data_, double speed, turns::const_iterator& current_turn, const turn& next_turn);
	std::string switch_lane(const data& data_/*, const turn& next_turn*/);
/*
msg_vector track_keymola(int tick, const data& data_);
msg_vector track_germany(const data& data_);
msg_vector track_usa(const data& data_);
msg_vector track_france(const data& data_);
*/
msg_vector do_track(int tick, const data& data_);

	std::string gameid;
	int tick;



//	double get_speed_limit(int lane, int piece,double distance,const turn& next_turn) const;

	bool turbo_available;

//keymola
	int lastpiece;
	double lastpos;

	int allow_changelane;  //switch solo en ticks pares. ///ticks impares tienen preferencia ordenes de throttle
	bool crashed;
	int crashes;
};

#endif
