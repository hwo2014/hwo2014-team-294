#include "protocol.h"

using namespace hwo_protocol;
using namespace std;

jsoncons::json hwo_protocol::make_request(const std::string& msg_type, const jsoncons::json& data) {
	jsoncons::json r;
	r["msgType"] = msg_type;
	r["data"] = data;
	return r;
}

jsoncons::json hwo_protocol::make_join_race(const std::string& name, const std::string& key, string track, int carcount) {
	jsoncons::json data;
	{
		jsoncons::json botId;
		botId["name"] = name;
		botId["key"] = key;
		data["botId"] = botId;
	}
	data["trackName"] = track;
//	data["trackName"] = "keimola";
//	data["trackName"] = "germany";
//	data["trackName"] = "usa";
//	data["trackName"] = "france";
	data["carCount"] = carcount;
	return make_request("joinRace", data);

}

jsoncons::json hwo_protocol::make_join(const std::string& name, const std::string& key) {
	jsoncons::json data;
	data["name"] = name;
	data["key"] = key;
	return make_request("join", data);


	{
		jsoncons::json botId;
		botId["name"] = name;
		botId["key"] = key;
		data["botId"] = botId;
	}
//	data["trackName"] = "keimola";
//	data["trackName"] = "germany";
	data["trackName"] = "usa";
//	data["trackName"] = "france";
	data["carCount"] = 1;
	return make_request("joinRace", data);

}

jsoncons::json hwo_protocol::make_ping() {
	return make_request("ping", jsoncons::null_type());
}

jsoncons::json hwo_protocol::make_throttle(double throttle) {
	return make_request("throttle", throttle);
}

jsoncons::json hwo_protocol::make_switch(const string& dest) { ///Left or Right 
	return make_request("switchLane", dest);
}

///ANIADIR MAS
vector<string> turbomsg={ 
"Pow pow pow pow pow",
"go home with your mother",
"bitch",
"cagon tus muelas",
};

jsoncons::json hwo_protocol::make_turbo() { ///Left or Right 
	int m=turbomsg.size();
	int n=rand()%m;
	string msg=turbomsg[n];
cout << "turbomsg: " << msg << endl;
	return make_request("turbo", msg);
}






