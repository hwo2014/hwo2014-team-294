#include "game_logic.h"
#include "protocol.h"
#include <iostream>
#include <iomanip>
#include <cassert>
#include <map>

using namespace hwo_protocol;
using namespace std;
using namespace jsoncons;

track _track;
turns _turns;


//http://www.reddit.com/r/HWO/comments/245srp/speed_dynamics/

double v1=0;
double v2=0;
double v3=0;
bool initial_calibration=true;
double k=0;
double m=0;


double terminal_velocity(double throttle) {
	return throttle/k;
}
double speed_in_next_ticks(double h, double v, int t) {
	return (v-(h/k))*pow(2.71828,(-k*t)/m)+(h/k);
}
int ticks_to_speed(double h, double v0, double v) { //How many ticks do you need to get to a given speed v
	return floor(( log((v-(h/k))/(v0-(h/k)))*m) / (-k))+1;
}

int distance_traveled(double h, double v0, int t) { //Distance traveled in t ticks:
	return ( m/k ) * ( v0 - ( h/k ) ) * ( 1.0 - pow(2.71828, ( ( -k*t ) / m ) ) + ( h/k ) * t );
}




game_logic::game_logic()
  : action_map
    {
      { "join", &game_logic::on_join },
      { "joinRace", &game_logic::on_join_race },
      { "gameStart", &game_logic::on_game_start },
      { "carPositions", &game_logic::on_car_positions },
      { "crash", &game_logic::on_crash },
      { "spawn", &game_logic::on_spawn },
      { "gameEnd", &game_logic::on_game_end },
      { "error", &game_logic::on_error },
      { "yourCar", &game_logic::your_car },
      { "gameInit", &game_logic::game_init },
      { "finish", &game_logic::finish },
      { "lapFinished", &game_logic::lapFinished },
      { "tournamentEnd", &game_logic::tournamentEnd },
      { "dnf", &game_logic::disqualified },
      { "turboAvailable", &game_logic::on_turbo_available },
      { "turboStart", &game_logic::on_turbo_start },
      { "turboEnd", &game_logic::on_turbo_end }
}, tick(0), lastpiece(0), lastpos(0), allow_changelane(0),turbo_available(false), crashed(false), crashes(0), turbonow(false) {
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
  const auto& msg_type = msg["msgType"].as<std::string>();
  const auto& data = msg["data"];
	if (msg_type=="carPositions") {
	  gameid = msg["gameId"].as<string>();
	 if (msg.has_member("gameTick")) {
	  tick = msg["gameTick"].as<int>();
	 }
	 else {
      cout << "expected tick: " << msg << endl;
	 }
	}
  auto action_it = action_map.find(msg_type);
  if (action_it != action_map.end())
  {
    return (action_it->second)(this, data);
  }
  else
  {
    std::cout << "Unknown message type: " << msg_type << std::endl;
    return { make_ping() };
  }
}

game_logic::msg_vector game_logic::your_car(const jsoncons::json& data)
{
  std::cout << "your_car" << std::endl;
  cout << data << endl;
  return { make_ping() };
}

///-------------------------------------------------piece
piece::piece(): _switch(false) {
}
piece::~piece() {
};

string piece::lane_pos_str(const lane_pos& lp) {
	switch(lp) {
		case lane_pos_central: return "central";
		case lane_pos_interior: return "interior";
		case lane_pos_exterior: return "exterior";
		default:
			cout << "l45678903" << endl;
			assert(false);
		
	}
}
	
std::string piece::piece_type_str(piece_type pt) {
	switch(pt) {
		case piece_type_straight: return "straight";
		case piece_type_curve: return "curve";
		default:
			cout << "l75678903" << endl;
			assert(false);
		
	}
}

////------------------------------------------------------------curve
curve::curve(double angle, double radius): _angle(angle), _radius(radius), _length_switch(0) {
	if (abs(_radius-100)<0.001) {
		_length_switch=81.0275; ///ver doc/piezas
	}
	else if (abs(_radius-200)<0.001) {
		_length_switch=81.0537; ///ver doc/piezas
	}
	else {
		double r=_radius;  
		double anglerad=abs(_angle)*2*3.141592/360;
		double d=anglerad*r;
		_length_switch=d;
	}
}

curve::piece_type curve::type() const { 
	return piece_type_curve; 
}

double curve::length(double lateral_pos, bool consideer_switch) const { 
	//if (_switch) pos=0; ///en un switch pongamos que la distancia es ni larga ni corta
	if (consideer_switch) return _length_switch;
	double r;
	if (_angle<0) {
		r=_radius+lateral_pos;  
	}
	else {
		r=_radius-lateral_pos;  
	}
	double anglerad=abs(_angle)*2*3.141592/360;
	double d=anglerad*r;
	return d; 
}

void curve::dump(const lanes& lanes_) const{
	cout << (_switch?"SWITCH; ":"");
	cout << "angle " << _angle << " radius " << _radius << " ";
	for (lanes::const_iterator i=lanes_.begin(); i!=lanes_.end(); ++i) {
		double lateral_pos=i->second.distance_from_center;
		cout << "; lane " << i->first << " {" << lane_pos_str(get_lane_pos(lateral_pos));
		cout << ", length " << length(lateral_pos,false) << "}";
	}
	if (_switch) cout << "; switch length " << _length_switch;
	cout << endl;
}

piece::lane_pos curve::get_lane_pos(double lateral_pos) const {
	if (abs(lateral_pos)<0.001) return lane_pos_central;
	if (lateral_pos<0 && _angle>0) return lane_pos_exterior;
	if (lateral_pos<0 && _angle<0) return lane_pos_interior;
	if (lateral_pos>0 && _angle<0) return lane_pos_exterior;
	if (lateral_pos>0 && _angle>0) return lane_pos_interior;
	cout << "x34578970 " << lateral_pos << endl;
	assert(false);
}

////------------------------------------------------------------straight
straight::straight(double length): _length(length), _length_switch(length+2.059) {
}
double straight::length(double lateral_pos, bool consideer_switch) const { 
	if (consideer_switch) return _length_switch; ///ver doc/piezas
	return _length; 
}

straight::piece_type straight::type() const { 
	return piece_type_straight; 
}

void straight::dump(const lanes& lanes_) const{
	cout << (_switch?"SWITCH; ":"");
	cout << "length " << _length;
	if (_switch) cout << "; length switch " << _length+2.059;
	cout << endl;
}

straight::lane_pos straight::get_lane_pos(double lateral_pos) const {
	if (abs(lateral_pos)<0.001) return lane_pos_central;
	if (lateral_pos<0) return lane_pos_exterior;
	if (lateral_pos>0) return lane_pos_interior;
	assert(false);
}

/////---------------------------------------------------------lanes
int lanes::max_pos() const {
	return size()-1;
}
void lanes::dump() const {
	for (const_iterator i=begin(); i!=end(); ++i) {
		cout << "lane #" << i->first << " distance from axe " << i->second.distance_from_center << endl;
	}
}
	


////////////-----------------------------------------------lane


lane::lane(): distance_from_center(0) {
}

////////////-----------------------------------------------turn
turn::turn(): distance(0), angle(0), radius(0), pieceid_start(-1), pieceid_end(-1) {
}

void turn::dump(string prefix) const {
	cout << prefix << "{pieces " << pieceid_start << "-" << pieceid_end << " dist " << distance << " angle " << angle << " radius " << radius;
	for (lanes::const_iterator i=_track._lanes.begin(); i!=_track._lanes.end(); ++i) {
		cout << "; lane " << i->first << " " << get_length(i->first);
	}
	cout << "}";
}

double turn::get_speed_limit(int lane, double Fslip) const {  ///cada turn tiene un speed limit (velocidad maxima a la entrada de la curva)
	if (Fslip<0.001) {
//		cout << endl << "XXFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF=0XXXXXXXXXXXXXXXXXXX" << endl;
		return 6;		
	}

//	if (angle<25) return 7; //un angulo pequenio de curva, sin limite

	const curve& curve_=dynamic_cast<const curve&>(*_track.find(pieceid_start)->second);
	double lane_radius=curve_.length(_track._lanes.find(lane)->second.distance_from_center,false);
	double Vmaxnoslip=sqrt(Fslip*lane_radius);
//cout << endl << "lane_radius: " << lane_radius << " " << Vmaxnoslip << " " << pieceid_start << endl;
	return Vmaxnoslip;

/*
	if (lane_radius<40) lane_radius=40;
	if (lane_radius>220) lane_radius=220;
	double radiusf=lane_radius/(220-40);
	double radiusw=0.8;
	radiusf*=radiusw;

	double iangle=angle;
	if (iangle>200) iangle=200;
	double anglef=1-iangle/200;  ///velocidad es maxima para angle 0, minima angle 200 o mas
	double anglew=1;
	anglef*=anglew;
	
	double vmin=5;
	double vmax=9;

	double v=vmin+(vmax-vmin)*radiusf*anglef;
	return v;
*/

/*
	if (lane_radius<70) { //curvas de radio 50
		if (angle<100) { //abiertas
			return 5.5;
		}
		else { //cerradas
			return 3.5;
		}
	}
	else { //curvas de mayor radio
		if (angle<100) { //abiertas
			return 7.0;
		}
		else { //cerradas
			return 5.5;
		}
	}
	return 100;  ///a tope
*/
}

double turn::get_length(int lane) const { 
	double lateral_pos=_track._lanes.find(lane)->second.distance_from_center;
	double r;
	if (angle<0) {
		r=radius+lateral_pos;  
	}
	else {
		r=radius-lateral_pos;  
	}
	double anglerad=abs(angle)*2*3.141592/360;
	double d=anglerad*r;
	return d; 
}

double turn::length_percent(int piece, double piece_distance , int lane) const {
	double total_len=get_length(lane);
	int p=pieceid_start;

	double lateral_pos=_track._lanes.find(lane)->second.distance_from_center;
	double l=0;
	while(true) {
		if (p==piece) {
			l+=piece_distance;
			break;
		}
		l+=_track.find(p)->second->length(lateral_pos,false);
		p=_track.next(p);

		if (l>1000000) { cout << "f57859095 " << endl; throw ""; }
	}
	return l/total_len;
}


bool turn::is_left() const {
	return angle<0;  //a derechas es positivo
}
bool turn::is_right() const {
	return angle>0;  //a derechas es positivo
}


void turns::dump() const {
	int n=0;
	for (const_iterator i=begin(); i!=end(); ++i) {
		cout << "#" << n++ << " ";
		i->dump("");
		cout << endl;
	}
}
turns::const_iterator turns::find(int piece) const {
	for (const_iterator i=begin(); i!=end(); ++i) {
		if (piece>=i->pieceid_start && piece<=i->pieceid_end) return i;
	}
	return end();
}

///*---------------------------------------------------------------------------

track::track() {
	}
track::~track() {
	for (iterator i=begin(); i!=end(); ++i) {
		delete i->second;
	}
}

track::switches track::get_switches() const {
	switches l;

	const_iterator sw0=find_switch(0); ///encontrar el primer switch desde la pieza 0
	if (sw0==end()) return l;

	const_iterator i=sw0;
	l.push_back(i);
	while(true) {
		i=next(i);//next piece
		const_iterator j=find_switch(i);
		if (j==sw0) { ///pieza switch inicial
			return l;
		}
		l.push_back(j);
		i=j;

	}

	assert(false);	
}

void track::switches::dump() const {
	for (const_iterator i=begin(); i!=end(); ++i) {

		track::const_iterator ti=*i;
		const_iterator j=i;
		++j; //sig. switch
		if (j==end()) j=begin(); //primer switch
	    track::const_iterator ti2=*j;
		int l=_track.shortest_lane(ti,ti2);
		cout << "piece " << ti->first << " lane " << l << endl;
	}
}


void track::dump() const {
	cout << "track " << name << endl;
	cout << _lanes.size() << " lanes:" << endl;
	_lanes.dump();

	cout << endl;
	cout << "switches:" << endl;
	get_switches().dump();

	cout << endl;

	int n=0;
	cout << "pieces:" << endl;
	for (const_iterator i=begin(); i!=end(); ++i) {
		cout << "piece #" << n++ << " "; 
		i->second->dump(_lanes);
	}

	cout << endl;
}

track::const_iterator track::find_switch(int startpiece, int endpiece) const {
//cout << "A" << startpiece << " " << endpiece << endl;
	const_iterator i=find(startpiece);
	int n=12000;
	while(true) {
		if (i->second->_switch) return i;

		++i;
		if (i==end()) i=begin();

		if (i->first>endpiece) return end();

		--n;
		if (n==0) { assert(false); }
	}
}



double track::get_length(int piece, int lanendx, bool using_switch) {
	double d=find(piece)->second->length(_lanes.find(lanendx)->second.distance_from_center,using_switch); 
//	cout << " > p " << piece << " d " << d << " " << using_switch << "< ";
	return d;
}



turns track::get_turns() const {
	turns ts;
	int piece_index=0;
	int lastp=0;
	int n=10000;
	while(true) {
		int lane=0;
		turn t=get_next_turn(lane, piece_index, 0);
		if (t.pieceid_start<lastp) {
			ts.push_back(t);
			break;
		}
		lastp=t.pieceid_start;
		piece_index=t.pieceid_start;
		ts.push_back(t);
		if (--n==0) { cout << "h67590321" << endl; throw ""; }
	}
	return ts;
}

turn track::get_next_turn(int ln, int piece_index, double piece_distance) const {
	double lateralpos=_lanes.find(ln)->second.distance_from_center;
	turn t;
	t.distance-=piece_distance;
	bool initial_angle=0;
	const_iterator current=find(piece_index);
	piece::piece_type current_type=current->second->type();
	int n=10000;
//cout << "-----------------------------" << endl;
//cout << "--" << endl;
//t.dump("turn");
//current->second->dump(_lanes);
	bool incurve=(current_type==piece::piece_type_curve);
	double incurve_angle=0;
	if (incurve) {
		const curve& curve_=dynamic_cast<const curve&>(*current->second);
		incurve_angle=curve_._angle;
	}

	while(true) {
		if (current->second->type()==piece::piece_type_straight) {
			t.distance+=current->second->length(lateralpos,false);
			incurve=false;
		}
		else { //curve
			const curve& curve_=dynamic_cast<const curve&>(*current->second);
			if (!incurve) break; ///es la curva vip
			//seguimos en la misma curva que empezamos?
			bool sign1=(incurve_angle<0);
			bool sign2=(curve_._angle<0);
			if (sign1==sign2) {
				t.distance+=current->second->length(lateralpos,false);
			}		
			else {
				break; ///empieza la curva vip
			}
		}
		++current;
		if (current==end()) current=begin();

		--n;
		if (n==0) {
			cout << "h78956009" << endl;
			throw "";
		}
	}	
	//estamos en la curva vip
	const curve& curve_=dynamic_cast<const curve&>(*current->second);
	double ang=curve_._angle;
	t.pieceid_start=std::distance(begin(), current);
	t.pieceid_end=t.pieceid_start;
//		curve_.dump(_lanes);
//		t.dump("turn");

	while(true) {
		if (current->second->type()==piece::piece_type_straight) {
			return t; ///hemos acabado la curva
		}
		else { //curve
			const curve& curve_=dynamic_cast<const curve&>(*current->second);
			bool sign1=ang<0;
			bool sign2=curve_._angle<0;
			if (sign1==sign2) {
				//t.distance+=current->second->length(lateralpos,false);
				if (abs(t.angle)<0.001) {
					t.angle=curve_._angle;
					t.radius=curve_._radius;
				}
				else {
					double prop=abs(curve_._angle)/(abs(t.angle)+abs(curve_._angle)); ///fraccion nnuevo fragmento con respecto al acumulado
					t.angle+=curve_._angle;
					t.radius=t.radius*(1-prop)+curve_._radius*prop;
				}
				t.pieceid_end=std::distance(begin(), current);
			}
			else {
				return t;//es ya la siguiente curva hacia el lado contrario
			}
		}
		++current;
		if (current==end()) current=begin();

		--n;
		if (n==0) {
			cout << "h78956009" << endl;
			throw "";
		}
	}
	cout << "m67859003 deadlock!!" << endl;
	return t;
}


///---------------------------------------------------------------------------	

/*
struct speed_limits: public map<int,double> { ///piece max_velocity
}
*/

//speed_limits _speed_limits;

/*
speed_limits get_speed_limits(const track::turns& t) {
	speed_limits sl;
	for (track::turns::const_iterator i=t.begin(); ;i!=t.end(); ++i) {
		const turn& o=*i;
		if (o.angle<30) continue;
		speed_limits::value_type(t.pieceid,
	}
	
}
*/

game_logic::msg_vector game_logic::tournamentEnd(const jsoncons::json& data) {
  std::cout << "tournamentEnd" << std::endl;
	cout << data << endl;
  return { make_ping() };
}


game_logic::msg_vector game_logic::disqualified(const jsoncons::json& data) {
  std::cout << "someone has been disqualified" << std::endl;
	cout << data << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::lapFinished(const jsoncons::json& data) {
  std::cout << "lapFinished" << std::endl;
	cout << data << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::finish(const jsoncons::json& data) {
  std::cout << "finish" << std::endl;
  cout << data << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& data) {
    if (!crashed) {
	  turbo_available=true;
	  std::cout << "turbo is available" << std::endl;
	}
	else {
	  std::cout << "turbo would be available if we were not wreckage" << std::endl;
	}
//  cout << data << endl;
  return { make_ping() };
}
game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& data) {
	if (data["name"]=="favedog") {
		turbonow=true;
	}
//  cout << data << endl;
  return { make_ping() };
}
game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& data) {
	if (data["name"]=="favedog") {
		turbonow=false;
	}
//  cout << data << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::game_init(const jsoncons::json& data) {
  std::cout << "game_init" << std::endl;
//	cout << data << endl;
  const json& trk=data["race"]["track"];
  _track.name=trk["id"].as<string>();

  const json& lns=trk["lanes"];
  for (int i=0; i<lns.size(); ++i) {
		lane l;
		int index=lns[i]["index"].as<int>();

		l.distance_from_center=lns[i]["distanceFromCenter"].as<double>();
		_track._lanes.insert(lanes::value_type(index,l));
  }

  const json& pcs=trk["pieces"];
  for (int i=0; i< pcs.size(); ++i) {
		bool hassw=pcs[i].has_member("switch");
		bool hasl=pcs[i].has_member("length");
		piece *p=0;
		if (hasl) {
			double length=pcs[i]["length"].as<double>();
			p=new straight(length);
		}
		else {
			double radius=pcs[i]["radius"].as<double>();
			double angle=pcs[i]["angle"].as<double>();
			p=new curve(angle,radius);
		}
		assert(p!=0);
		p->_switch=hassw;
		_track.insert(track::value_type(i,p));
  }

  _turns=_track.get_turns();


  cout << "turns:" << endl;
  _turns.dump();
  _track.dump();


//  _speed_limits=set_speed_limits();;


  return { make_ping() };
}


game_logic::msg_vector game_logic::on_join(const jsoncons::json& data)
{
  std::cout << "Joined game " << gameid << std::endl;
  //cout << data << endl; //{"key":"**************","name":"favedog"}
  return { make_ping() };
}
game_logic::msg_vector game_logic::on_join_race(const jsoncons::json& data)
{
  std::cout << "Joined race " << gameid << std::endl;
 cout << data << endl; 
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& data)
{
  std::cout << "Race started" << std::endl;


  cout << data << endl;
  return { make_ping() };
}


ostream& operator << (ostream& os, const data& d) {
	os << "lap " << d.lap << " piece " <<  d.piece_index << " d " << d.piece_distance << "; lane " << d.start_lane_index << ">" << d.end_lane_index << "; ANG:" << d.angle;
	return os;
}

#include <set>
/*
struct state {
	state(): lap(0), piece(0), dist(0), lane(0), v(0), used_switch(false) {
	}
	bool operator < (const pos& other) const {
		if (lap!=other.lap) return lap<other.lap;
		if (piece!=other.piece) return piece<other.piece;
		return dist<other.dist;
	}
	int lap;
	int piece;
	double dist;

	int lane;
	double v;
	bool used_switch;
};
struct trajectory: public std::set<state> {

};

trajectory trj;
*/

//double throttle=0;

#include <fstream>
ofstream vlog("v");


/*
double game_logic::get_speed_limit(int lane, int piece, double distance, const turn& next_turn) const {
	piece::lane_pos lp=_track.find(piece)->second->get_lane_pos(_track._lanes.find(lane)->second.distance_from_center);
cout << "; speed limit for " << piece::lane_pos_str(lp);
	double limit=100;

	if (next_turn.radius<60) return 6;

	if (next_turn.angle>30 && next_turn.distance<100) {  //estamos a punto de entrar en curva de mas de 30 grados
		return 6;
	}

	turns::const_iterator t=_turns.find(piece);
	if (t==_turns.end()) return 100; ///no estoy en ninguna curva
	return 6;  ///estsmos en una curva

cout << "=" << limit;

	return limit;	
}
*/

track::const_iterator track::find_switch(int piecedesde) const {
	return find_switch(find(piecedesde));
}

track::const_iterator track::find_switch(const_iterator piecedesde) const {
	int m=size();
	while(true) {
		if (piecedesde->second->_switch) return piecedesde;
		++piecedesde;
		if (piecedesde==end()) piecedesde=begin();

		if (--m==0) break;		///useful if there is no switches in the track
	}
	return end();
}

int track::next(int l) const {
	++l;
	if (l==size()) l=0;
	return l;
}


track::const_iterator track::next(const_iterator p) const {
	++p;
	if (p==end()) p=begin();
	return p;
}

int track::shortest_lane(const_iterator from, const_iterator to) const {
	lanesdist lanedist_=get_lanesdist(from,to);
	double dist=10000000;
	int ln=-1;
	for (lanesdist::const_iterator i=lanedist_.begin(); i!=lanedist_.end(); ++i) {
//cout << i->first << " " << i->second << endl;
		if (i->second <= dist-0.001) { ///si son = prefiero el lane de numeracion mas alta , es mas interior
			dist=i->second;
			ln=i->first;
		}
	}
	assert(ln!=-1);
	return ln;
}

/*
int track::shortest_lane(const_iterator from, const_iterator to) const {

	typedef map<int,double> lanesdist;
	lanesdist lanedist_;
	for (lanes::const_iterator i=_lanes.begin(); i!=_lanes.end(); ++i) {
		lanedist_[i->first]=0;		
	}

	const_iterator p=from;
	while(true) {
		for (lanes::const_iterator l=_lanes.begin(); l!=_lanes.end(); ++l) {
			lanedist_[l->first]+=p->second->length(l->second.distance_from_center,false);
		}
	}

	double dist=10000000;
	int ln=-1;
	for (lanesdist::const_iterator i=lanedist_.begin(); i!=lanedist_.end(); ++i) {
		if (i->second>dist < dist) {
			dist=i->second>dist;
			ln=i->first;
		}
	}
	assert(ln!=-1);
	return ln;
}
*/
void track::lanesdist::dump() const {
	for (const_iterator i=begin(); i!=end(); ++i) {
		cout << "lane " << i->first << " len " << i->second << endl;
	}
}

track::lanesdist track::get_lanesdist(const_iterator from, const_iterator to) const {
	lanesdist lanedist_;
	for (lanes::const_iterator i=_lanes.begin(); i!=_lanes.end(); ++i) {
		lanedist_[i->first]=0;		
	}
//cout << "dist form " << from->first << " to " << to->first << endl;
	const_iterator p=from;
	int n=10000;
	while(true) {
		for (lanes::const_iterator l=_lanes.begin(); l!=_lanes.end(); ++l) {
			double d=p->second->length(l->second.distance_from_center,false);
//			cout << " lane " << l->first << " dist " << d;
			lanedist_[l->first]+=d;
//			cout << " total lane " << lanedist_[l->first] << endl;

		}
		p=next(p);
		if (p==to) break;

		if (--n==0) { cout << "n56748932" << endl; assert(false); }
	}
	return lanedist_;
}

string game_logic::switch_lane(const data& data_/*, const turn& next_turn*/) { ///empty if no need to switch

  if (allow_changelane>0) {
		--allow_changelane;
		return "";
  }
  allow_changelane=4; ///convenio, solo se cambia de carril en ticks multiplos de 4

	//si la siguiente curva es a derechas y hay un cambio entre el coche y la curva tomar a carril derecho ) 
	//?podria valer algo como: si la siguiente curva es a derechas y hay un cambio entre el coche y la curva tomar a carril derecho, y lo mismo con una curva a izquierdas
	//bool is_left=next_turn.is_left();
	track::const_iterator next_switch=_track.find_switch(data_.piece_index);
	track::const_iterator next_next_switch=_track.find_switch(_track.next(next_switch));
	///hay que saber que lane es mas corta
	int shortest_lane_=_track.shortest_lane(next_switch,next_next_switch);

	if (data_.end_lane_index==shortest_lane_) {
		return "";
	}
	if (data_.end_lane_index<shortest_lane_) {
		return "Right";
	}
	return "Left";

/*

	if (s!=_track.end()) {
		//const swpiece&
		if (is_left) {
			if (data_.end_lane_index>0) {
				return "Left";
			} 
		}
		else {
			if (data_.end_lane_index<_track._lanes.max_pos()) {
				return "Right";
			}
		}
	}
*/

	return "";
}




struct record {
	double pos;
	double angle;
	double throttle;
};

struct records: public map<int , record> { ///indexed by piece
	const record& get_record(int piece) const {
		return find(piece)->second;
	}
};





records _laptime;

//turn::iterator _current_turn;

double Fslip=0;
bool recalculate_Fslip=true;
//double last_angle=0;

//double param_frenado=400;  ///si me paso tengo que reajustar este parametro

double get_brake_distance(double v0, double v1) {
	int t=ticks_to_speed(0,v0,v1);
	double dist=distance_traveled(0,v0,t);
	return dist;


//	if (v>8) v=8;
//	double brake_distance=param_frenado*v/8;
}



bool voy_pasao=false;
int voy_pasao_turn=-1;
int voy_pasao_processed_turn=-1;

bool voy_corto=false;
int voy_corto_turn=-1;
int voy_corto_processed_turn=-1;


double game_logic::throttle(const data& data_, double v, turns::const_iterator& current_turn, const turn& next_turn) {


 //  * punto en la pista    X-------d----------->X maximum speed here
/*
	v
	d
	vmax at (here+d)

	si distancia de frenado es < d throtthl a 0	

*/



	double angle=data_.angle;

	const piece& p=*_track.find(data_.piece_index)->second;
/*
	if (p.type()==piece::piece_type_straight) {
		if (abs(angle)<0.001) {
			recalculate_Fslip=true;
		}
	}
*/
//if (p.type()==piece::piece_type_straight && abs(angle)<0.001) {
//	recalculate_Fslip=true;
//}

if (recalculate_Fslip) {
	if (abs(angle)>0.001) {
//		cout << "________________________________________Fslip" << endl;
		double A=abs(angle); //-abs(last_angle);
		cout << endl;
		cout << "v: " << v;
		cout << " angle: " << A;
		if (p.type()==piece::piece_type_curve) {
			const curve& cu=dynamic_cast<const curve&>(p);
			double R=cu._radius;
			cout << " corner radius: " << R ;
			double B=(180/3.141592)*(v/R);
			cout << " angular velocity (deg): " << B;
			double Bslip=B-A;
			cout << " max angular velocity : " << Bslip;
			double Vthres=(3.141592/180)*Bslip*R;
			cout << " threshold speed : " << Vthres;
			Fslip=Vthres*Vthres/R;
			cout << " max cetrifugal force bvefore sliding : " << Fslip << endl;
			recalculate_Fslip=false;
		}
	}
}



/*
		///en el turn
		if (voy_pasao && voy_pasao_processed_turn!=current_turn->id()) { //el detector de pasao funciona con el current turn
			param_frenado+=20;  //mas distancia de frenado requierida
			cout << " DF> Se aumenta la distancia de frenado " << param_frenado << endl;
			voy_pasao_processed_turn=current_turn->id();
		}
		if (voy_corto && voy_corto_processed_turn!=next_turn.id()) {  ///el detector de corto funciona con el next turn
			param_frenado-=20;  //mas distancia de frenado requierida
			cout << " DF> Se disminuye la distancia de frenado " << param_frenado << endl;
			voy_corto_processed_turn=next_turn.id();
		}
*/

/*		
		if (voy_corto) {
			param_frenado-=20;
		}
*/
//last_angle=angle;

	double throttle=1;
//	_current_turn=_turns.end();
/*
// forma helena viernes 25 -abril. throttle
    track::const_iterator next_piece=_track.find(data_.piece_index);
    ++next_piece;
    if (next_piece==_track.end()) {
		next_piece=_track.begin();
	}
	if (next_piece->second->type()==piece::piece_type_straight) {
		throttle=1;
	}
	if (next_piece->second->type()==piece::piece_type_curve) {
		throttle=0.5;
	}
*/	

	//?hay un switch entre yo y la curva?
	int shortest_lane_=data_.end_lane_index;

	track::const_iterator next_switch=_track.find_switch(data_.piece_index);
	track::const_iterator next_next_switch=_track.find_switch(_track.next(next_switch));
	if (_track.get_index(next_switch)<=next_turn.pieceid_start) {
		shortest_lane_=_track.shortest_lane(next_switch,next_next_switch);
	}

	double next_v=next_turn.get_speed_limit(shortest_lane_,Fslip);

//--------------------------------------------------- throttle
	double throttle_max=1;

	double vlimit=100;
	double perc=0;

	if (current_turn!=_turns.end()) {
		vlimit=current_turn->get_speed_limit(data_.end_lane_index,Fslip);
		perc=current_turn->length_percent(data_.piece_index, data_.piece_distance ,data_.end_lane_index);

		cout << "; curve " << perc << "%";

		if (v>vlimit+0.5 && perc<0.45) {  ///estamos en la primera 45% de la curva
			throttle_max=0.5;  ///
			cout << "; ACELERAOS VAMOS ";
			return throttle_max;
		}
		if (v<vlimit-0.5) {  
			throttle_max=1;  ///
			cout << "; EMPANAOS VAMOS ";
	
		}
	}


	if (v<next_v && next_turn.distance>50) {
		cout << "; VAMOS CORTOS en la frenada; desaprovechados " << next_turn.distance;
		voy_corto=true;
		voy_corto_turn=next_turn.id();
	}


//	bool incurve=(next_turn.current_type==piece::piece_type_curve);

	if (data_.lap==0) {
		///set conservative speed limits
	}



	double delta_v=next_v-v;
	if (delta_v<0) {
		///si voy muy follado aumento la distancia de seguridad
		double brake_distance=get_brake_distance(v,next_v);


//		cout << endl << "param_frenado " << param_frenado << endl;

		if (next_turn.distance<brake_distance) { //distancia de frenado a capon
			throttle_max=0;
			cout << "; brake! " << delta_v << " in " << next_turn.distance;
		}
		
	}

	


// forma marcos viernes 25 -abril. throttle

	///+++++++++++++++++++++++++++++++++++++++
	///+++++++++++++++++++++++++++++++++++++++

	///+++++++++++++++++++++++++++++++++++++++
//	if (incurve) throttle_max=0.6;
	///+++++++++++++++++++++++++++++++++++++++


//	if (!incurve && next_turn.distance>100) throttle_max=0.8;
	

	throttle=throttle_max;


//	double max_angle=50;
//	if (abs(angle)>10) {
//		throttle=0;
//	}
/*
	else if (abs(data_.angle)<max_angle) {
		throttle=throttle_max-throttle_max*(abs(angle)/max_angle);
	}
*/


//	throttle=throttle_max;
	
/*
	double vmax=get_speed_limit(data_.end_lane_index,data_.piece_index,piece_distance,next_turn);
	if (v>vmax) {
		throttle=0;
	}
*/


//	_laptime.insert(records::value_type(piece_index,piece_distance



	if (throttle<0) throttle=0;
	if (throttle>1) throttle=1;
	return throttle;
}

int last_lap=0;
int last_piece=0;
int last_startlane=0;
int last_endlane=0;
double last_distance=0;

double game_logic::speed(const data& data_) {
	lastpiece=data_.piece_index;
	lastpos=data_.piece_distance;
	double piece_index=data_.piece_index;
	double piece_distance=data_.piece_distance;
	double v=0;
	if (last_piece==piece_index) {
		v=piece_distance-last_distance; ///1 tick as time magnitude
	}
	else {
		bool last_piece_used_switch=(last_startlane!=last_endlane);
/*
		if (last_lap==data_.lap) {
			cout << "; " << piece_index << " " << last_piece << endl;
			assert(piece_index==last_piece+1);   ///fail after a crash
		}
*/
//		double l=_track.find(last_piece)->second->get_length(_track.lanes.find(data_.end_lane_index)->second.distance_from_center,last_piece_used_switch); //longitud recorrida en la pieza anterior
		double l=_track.get_length(last_piece,last_endlane,last_piece_used_switch);
//cout << "; length piece " << last_piece << " " << l;
		l-=last_distance; ///distancia recorrida en la pieza anterior desde el ultimo tick
//cout << "; last_distance " << last_distance << "; l " << l;
		l+=piece_distance; ///distancia recorrida en la pieza actual
		v=l; ///1 tick as time magnitude
	}

	last_lap=data_.lap;
	last_piece=piece_index;
	last_distance=piece_distance;
	last_startlane=data_.start_lane_index;
	last_endlane=data_.end_lane_index;
	return v;
}






#include <math.h>
//======================================================================================================================do_track  \/
game_logic::msg_vector game_logic::do_track(int tick, const data& data_) {
//if (data_.lap>1) {
// cout << "g78699708" << endl;
// throw "";
//}
	cout << fixed << setprecision(2);

	cout << data_;
	double v=speed(data_);

if (initial_calibration) {
	double h=1;
	cout << endl << "initial_calibration" << endl;
	if (v<0.0000001) { 
		cout << "v still " << v << " at tick " << tick << endl;
		return { make_throttle(h) };
	}  ///wait till starts moiving
	if (v1==0) { 
		v1=v;
		cout << "got v1 " << v1 << " at tick " << tick << endl;
		return { make_throttle(h) }; 
	}
	if (v2==0) { 
		v2=v;
		cout << "got v2 " << v2 << " at tick " << tick << endl;
		return { make_throttle(h) }; 
	}
	if (v3==0) { 
		v3=v; 
		cout << "got v3 " << v3 << " at tick " << tick << endl;
	}
	initial_calibration=false;
	k=(v1-(v2-v1))/(v1*v1*h);
	m=1.0/(log((v3-(h/k))/(v2-(h/k)))/(-k));
	cout << endl;
	cout << "drag: " << k << " mass: " << m << endl;
	cout << "predicted speed for tick " << tick+10 << ":" << speed_in_next_ticks(h,v,tick+10) << endl;
	cout << "predicted to achieve v " << v+1 << " in tick " << tick+ticks_to_speed(h,v,v+1) << endl;
	cout << endl;
	
	//assert(false);
}
//DSISTANCIA DE FRENADO


	double vmax=100;
	double curve_percent=0;
	turns::const_iterator current_turn=_turns.find(data_.piece_index);
	if (current_turn!=_turns.end()) {
		curve_percent=current_turn->length_percent(data_.piece_index,data_.piece_distance,data_.end_lane_index);
		vmax=current_turn->get_speed_limit(data_.end_lane_index,Fslip);
	}

	cout << "; v/vmax " << v << "/" << vmax << "curve_percent: " << curve_percent << "%";

	double throttle_=0.5;
	turn next_turn=_track.get_next_turn(data_.end_lane_index,data_.piece_index,data_.piece_distance);

	if (v>vmax+0.5) {
		if (curve_percent<0.20) {  ///podemos acelerar superado el tramo donde el control de velocidad aplica (20% inicial)
			cout << "; PASAOOOOOO!";
			voy_pasao=true;
			voy_pasao_turn=current_turn->id();
			throttle_=0;
		}
	}
	else {
		cout << ";S ";
		throttle_=throttle(data_,v,current_turn,next_turn);
	}

	string swcommand=switch_lane(data_/*,next_turn*/);


//	vlog << tick << " " << (data_.piece_index/10) << " " << v << " " << data_.angle << endl;

	vlog << tick << " " << (data_.piece_index/10) << " " << v << " " << (vmax>99?0:vmax) << endl;

  cout << "; command: ";

	if (turbo_available) {
//  cout << "A ";
		if (current_turn==_turns.end()) { //voy en recta
//  cout << "B " << next_turn.distance;
		  if (next_turn.distance>500) {
//  cout << "C ";
			cout << "turbo!" << endl;
			turbo_available=false;
			return { make_turbo() };
		  }
		}
	}
	



  if (swcommand.empty()) {
	cout << "set throttle to " << throttle_ << endl;
	if (current_turn!=_turns.end()) {
		 current_turn->dump(">                  current turn:");
	}
	else {
		cout << ">                  current turn:";
	}
	cout << endl;
    next_turn.dump(">                     next turn:"); cout << endl;
	cout << endl;

    return { make_throttle(throttle_) };
  }
  else {
	cout << "switch to " << swcommand << " lane" << endl;
	cout << ">" << endl;
	cout << ">" << endl;
	cout << endl;
    return { make_switch(swcommand) };
  }

}
//======================================================================================================================do_track /\

//	throttle=0.6;
//	cout << "set throttle to " << throttle << endl;
 //   return { make_throttle(throttle) };
 
/*
	while(true) {
		if (piece_index==2 || piece_index==3) {  ///sw1. esta en p3
			if (data_.end_lane_index==0 && allow_changelane) { swcommand="Right"; break; }
		}

		if (piece_index==7 || piece_index==8) {  ///sw2. esta en p8
			if (data_.end_lane_index==1 && allow_changelane) { swcommand="Left"; break; }
		}

		if (piece_index==17 || piece_index==18) {  ///sw4. esta en p18
			if (data_.end_lane_index==0 && allow_changelane) { swcommand="Right"; break; }
		}

	
//		if (piece_index==34 || piece_index==35) { ///sw7. esta en p35
//			if (end_lane_index==1 && allow_changelane) { swcommand="Left"; break; }
//		}
	
	break;
}


*/


/*
	while(true) {
		if (data_.lap==0) {
			if (piece_index==0 && piece_distance<25) throttle=0.5+2*piece_distance/100;  ///0.50>   0.51 crash 
		}
		else {
			if (piece_index>=3) throttle=0.05;  ///caske: trottle=0.7 angulo= 59.9304 6.627744
		}

		if (data_.lap==0) {
			if (piece_index>=3) throttle=0.50;  ///0.50>   0.51 crash 
		}
		else {
			if (piece_index>=3) throttle=0.05;  ///caske: trottle=0.7 angulo= 59.9304 6.627744
		}
		if (piece_index==2 || piece_index==3) {  ///sw1. esta en p3
			if (data_.end_lane_index==0 && allow_changelane) { swcommand="Right"; break; }
		}

		if (data_.lap==0) {
			if (piece_index>=6) throttle=0.64;  ///0.64>  0.65  crash 
		}
		else {
			if (piece_index>=6) throttle=0.55;  ///0.55>    crash 
		}

		if (piece_index>=7) throttle=1; 
 
		if (piece_index==7 || piece_index==8) {  ///sw2. esta en p8
			if (data_.end_lane_index==1 && allow_changelane) { swcommand="Left"; break; }
		}

		if (data_.lap==0) {
			if (piece_index>=10) throttle=0.73;   ///0.73>   0.74 crash
		}
		else {
			if (piece_index>=10) throttle=0.72;   ///0.72>   0.73 crash
		}
		if (piece_index>=12) throttle=0.80;  ///0.80>   0.81 crash
		if (piece_index>=13) throttle=0.56;  ///0.56>  0.57 crash
		if (piece_index>=14) throttle=0.25;   ///0.25> 0.26 crash
		if (piece_index>=16) throttle=1;  
		if (piece_index>=18) throttle=0.55;  ///0.55> 0.56 crash

		if (piece_index==17 || piece_index==18) {  ///sw4. esta en p18
			if (data_.end_lane_index==0 && allow_changelane) { swcommand="Right"; break; }
		}

		if (data_.lap==0) {
			if (piece_index>=21) throttle=0.79;  ///0.80> 0.80 crash
		}
		else {
			if (piece_index>=21) throttle=0.79;  ///0.79> 0.80 crash
		}

		if (piece_index>=25) throttle=0.58;  ///0.58>  0.60 crash
	//	if (piece_index==24 || piece_index==25) {  ///sw5. esta en p25
	//		if (end_lane_index==0 && allow_changelane) { swcommand="Right"; break; }
	//	}
		if (piece_index>=27) throttle=0.61;  ///0.61>  0.62 crash
		if (piece_index>=29) throttle=0.58;  ///0.58> 0.59 crash
		if (piece_index>=33) throttle=1;  

		if (piece_index>=38) {
			if (turbo_available) {
//				cout << "turbo!" << endl;
//				turbo_available=false;
//				return { make_turbo() };
			}
		}
	
//		if (piece_index==34 || piece_index==35) { ///sw7. esta en p35
//			if (end_lane_index==1 && allow_changelane) { swcommand="Left"; break; }
//		}
	
	break;
  }
*/



game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& dta) {
// cout << "on_car_positions: " << endl;
// cout << data << endl;



	data data_;


//	cout << data[0] << endl;
  data_.angle=dta[0]["angle"].as<double>();
  data_.piece_distance=dta[0]["piecePosition"]["inPieceDistance"].as<double>();
  data_.start_lane_index=dta[0]["piecePosition"]["lane"]["startLaneIndex"].as<int>();
  data_.end_lane_index=dta[0]["piecePosition"]["lane"]["endLaneIndex"].as<int>();
  data_.lap=dta[0]["piecePosition"]["lap"].as<int>();
  data_.piece_index=dta[0]["piecePosition"]["pieceIndex"].as<int>();




///CPM: car performance model
//vmax  velocidad maxima. poner throtthel en la pos maxima y determinar vmax

    cout << " tick " << tick << " ";

	msg_vector ans;

	ans=do_track(tick,data_);

	if (crashed) {
		cout << "."; cout.flush();
		return { make_ping() };
	}

	return ans;

}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& data) {
//  std::cout << "Someone crashed" << data << std::endl;
	string who=data["name"].as<string>();
	if (who=="favedog") {
	++crashes;

  std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << std::endl;
  std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX CRASH " << crashes << " XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << std::endl;
  std::cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << std::endl;
	crashed=true;
	}
	else {
	  std::cout << "good! opponent crashed! :) " << who << std::endl;
	}

  cout << data << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& data) {
	string who=data["name"].as<string>();
	if (who=="favedog") {
		crashed=false;
		cout << endl << "resumed" << endl;
	}
	else {
	  std::cout << "opponent resumed. " << who << std::endl;
	}

//  std::cout << "Someone spawned" << std::endl;
// cout << data << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& data)
{
  std::cout << "Race ended" << std::endl;
  cout << data << endl;
  return { make_ping() };
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& data)
{
  std::cout << "Error: " << data.to_string() << std::endl;
  cout << data << endl;
  return { make_ping() };
}
